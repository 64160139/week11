package com.fah.week11;

public class Bird extends Animal implements Flyable {

    public Bird(String name) {
        super(name,2);
    }

    @Override
    public void eat() {
        System.out.println(toString() + " eat .");
        
    }

    @Override
    public void sleep() {
        System.out.println(toString() + " sleep .");
        
    }
    @Override
    public String toString() {
        return "Bird(" + this.getName() + ")";
    }

    @Override
    public void fly() {
        System.out.println(this + " fly .");
        
    }

    @Override
    public void takeof() {
        System.out.println(this + " takeof .");
    }

    @Override
    public void landing() {
        System.out.println(this + " landing .");
        
    }
}
