package com.fah.week11;

public class Human extends Animal implements walkable {

    public Human(String name) {
        super(name, 2);

    }

    @Override
    public void eat() {
        System.out.println(this + " eat .");

    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep .");

    }

    @Override
    public String toString() {
        return "Human(" + this.getName() + ")";
    }

    @Override
    public void walk() {
        System.out.println(this + " walk .");
        
    }

    @Override
    public void run() {
        System.out.println(this + " run .");
        
    }

}
