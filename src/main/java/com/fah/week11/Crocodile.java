package com.fah.week11;

public class Crocodile extends Animal implements Crawlable , Swimable{

    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public void eat() {
        System.out.println(this + "eat .");

    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep .");

    }

    @Override
    public String toString() {
        return "Crocodile(" + this.getName() + ")";
    }

    @Override
    public void crawl() {
        System.out.println(this + " Crawl .");
    }

    @Override
    public void swim() {
        System.out.println(this + " Swim .");
    }

}
