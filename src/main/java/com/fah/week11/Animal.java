package com.fah.week11;

public abstract class Animal {
    private String name;
    private int numberOfleg;

    public Animal(String name, int numberOfleg) {
        this.name = name;
        this.numberOfleg = numberOfleg;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfleg() {
        return numberOfleg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumberOfleg(int numberOfleg) {
        this.numberOfleg = numberOfleg;
    }

    @Override
    public String toString() {
        return "Animal(" + name + ") has " + numberOfleg + " Legs";
    }
    public abstract void eat();
    public abstract void sleep();
}
