package com.fah.week11;


public class App 
{
    public static void main( String[] args )
    {
        Bat bat1 = new Bat("Luecous");
        bat1.eat();
        bat1.sleep();
        bat1.fly();
        bat1.takeof();
        bat1.landing();
        System.out.println("------------------------");
        //------------------------/
        Fish fish1 = new Fish("Nari");
        fish1.eat();
        fish1.sleep();
        fish1.swim();
        System.out.println("------------------------");
        //------------------------/
        Rat rat1 = new Rat("Tato");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();
        System.out.println("------------------------");
        //------------------------/
        Cat cat1 = new Cat("karw");
        cat1.eat();
        cat1.sleep();
        cat1.walk();
        cat1.run();
        System.out.println("------------------------");
        //------------------------/
        Dog dog1 = new Dog("Super dog");
        dog1.eat();
        dog1.sleep();
        dog1.walk();
        dog1.run();
        System.out.println("------------------------");
        //------------------------/
        Human human1 = new Human("Fah007");
        human1.eat();
        human1.sleep();
        human1.walk();
        human1.run();
        System.out.println("------------------------");
        //------------------------/
        Snake snake1 = new Snake("Narkie");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();
        System.out.println("------------------------");
        //------------------------/
        Crocodile cro = new Crocodile("kake");
        cro.eat();
        cro.sleep();
        cro.crawl();
        cro.swim();
        System.out.println("------------------------");
        //------------------------/
        Plane far = new Plane("Farfaraway","airline");
        far.fly();
        far.takeof();
        far.landing();
        System.out.println("------------------------");
        //------------------------/
        Bird bird1 = new Bird("Zato");
        bird1.eat();
        bird1.sleep();
        bird1.fly();
        bird1.takeof();
        bird1.landing();
        System.out.println("------------------------");
        //------------------------/
        submarine parwit = new submarine("parrrr", "phasie");
        parwit.swim();
        System.out.println("------------------------");
        //------------------------/
        walkable[] walkablesObjects ={rat1,human1,dog1,cat1};
        for (int i = 0; i < walkablesObjects.length; i++) {
            walkablesObjects[i].walk();
            walkablesObjects[i].run();
        }
        System.out.println("----------All walkable------------");
        //------------------------/
        Flyable[] flyObjects = {bat1,far,bird1};
        for (int i = 0; i < flyObjects.length; i++) {
            flyObjects[i].fly();
            flyObjects[i].takeof();
            flyObjects[i].landing();
        }
        System.out.println("----------All Flyable------------");
        //------------------------/
        Swimable[] swimObjects = {fish1,parwit,cro};
        for (int i = 0; i < swimObjects.length; i++) {
            swimObjects[i].swim();
        }
        System.out.println("----------All Swimable------------");
        //------------------------/
        Crawlable[] crawlablesObjects = {snake1,cro};
        for (int i = 0; i < crawlablesObjects.length; i++) {
            crawlablesObjects[i].crawl();
        }
        System.out.println("----------All Crawlable------------");
        //------------------------/
    }
}
