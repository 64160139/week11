package com.fah.week11;

public class Plane  extends Vahicle implements Flyable {

    public Plane(String name, String engineName) {
        super(name, engineName);
    }
    @Override
    public String toString() {
        return "Plane("+this.getName()+")" + " EngineName : "+ this.getEngineName();
    }

    @Override
    public void fly() {
        System.out.println(this + " fly .");
        
    }

    @Override
    public void takeof() {
        System.out.println(this + " takeof .");
        
    }

    @Override
    public void landing() {
        System.out.println(this + " landing .");
    }
    
}
